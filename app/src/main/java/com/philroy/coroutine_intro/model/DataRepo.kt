package com.philroy.coroutine_intro.model

import android.graphics.Color
import com.philroy.coroutine_intro.R
import com.philroy.coroutine_intro.model.remote.DataApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import kotlin.random.Random

object DataRepo {

    private val dataApi = object : DataApi {
        override suspend fun getLifecycles(): List<Pair<String, String>> {
            return listOf(
                Pair("onCreate", "Fragment CREATED"),
                Pair("onCreateView", "Fragment CREATED and View INITIALIZED"),
                Pair("onViewCreated", "Fragment CREATED and View INITIALIZED"),
                Pair("onViewStateRestored", "Fragment and View CREATED"),
                Pair("onStart", "Fragment and View STARTED"),
                Pair("onResume", "Fragment and View RESUMED"),
                Pair("onPause", "Fragment and View STARTED"),
                Pair("onStop", "Fragment and View CREATED"),
                Pair("onSaveInstanceState", "Fragment and View CREATED"),
                Pair("onDestroyView", "Fragment CREATED and View DESTROYED"),
                Pair("onDestroy", "Fragment DESTROYED"),
                )
        }

        override suspend fun getRandomColor(): Int {
            return Color.rgb(Random.nextInt(255), Random.nextInt(255), Random.nextInt(255))
        }
    }

    suspend fun getLifecycles(): List<Pair<String, String>> = withContext(Dispatchers.IO) {
        delay(400L)
        return@withContext dataApi.getLifecycles()
    }

    suspend fun getRandomColor():Int = withContext(Dispatchers.IO) {
        delay(400L)
        return@withContext dataApi.getRandomColor()
    }
}