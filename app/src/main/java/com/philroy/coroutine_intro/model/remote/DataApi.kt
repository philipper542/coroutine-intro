package com.philroy.coroutine_intro.model.remote

interface DataApi {
    suspend fun getRandomColor():Int
    suspend fun getLifecycles():List<Pair<String, String>>
}