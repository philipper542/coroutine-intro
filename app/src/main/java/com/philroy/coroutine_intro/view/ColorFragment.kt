package com.philroy.coroutine_intro.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.philroy.coroutine_intro.databinding.FragmentColorBinding
import com.philroy.coroutine_intro.viewmodel.DataViewModel

class ColorFragment : Fragment() {

    private var _binding: FragmentColorBinding? = null
    private val binding get() = _binding!!
    private val dataViewModel by viewModels<DataViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentColorBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }
    fun initListeners() = with(binding){
        btnRetrieve.setOnClickListener {
            dataViewModel.getRandomColor()
        }

        btnNext.setOnClickListener {
            val action = ColorFragmentDirections.actionColorFragmentToLifecycleFragment()
            findNavController().navigate(action)
        }

        dataViewModel.randomColor.observe(viewLifecycleOwner) { color ->
            cvColor.setCardBackgroundColor(color)
            cvColor.setOnClickListener {
                val action = ColorFragmentDirections.actionColorFragmentToSpecificColorFragment(color)
                findNavController().navigate(action)
            }
        }
    }
}