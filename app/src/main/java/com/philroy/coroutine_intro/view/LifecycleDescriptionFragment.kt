package com.philroy.coroutine_intro.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.philroy.coroutine_intro.databinding.FragmentLifecycleDescriptionBinding

class LifecycleDescriptionFragment : Fragment() {

    private var _binding: FragmentLifecycleDescriptionBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLifecycleDescriptionBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val args: LifecycleDescriptionFragmentArgs by navArgs()
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleName.text = args.lifecycleName
        binding.lifecycleDescription.text = args.lifecycleDescription
    }
}