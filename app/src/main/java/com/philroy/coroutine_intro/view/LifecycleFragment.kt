package com.philroy.coroutine_intro.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.philroy.coroutine_intro.databinding.FragmentLifecycleBinding
import com.philroy.coroutine_intro.viewmodel.DataViewModel

class LifecycleFragment : Fragment() {

    private var _binding: FragmentLifecycleBinding? = null
    private val binding get() = _binding!!
    private val dataViewModel by viewModels<DataViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLifecycleBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    fun initListeners() = with(binding){
        btnRetrieve.setOnClickListener {
            dataViewModel.getLifecycles()
        }

        btnBack.setOnClickListener {
            val action = LifecycleFragmentDirections.actionLifecycleFragmentToColorFragment()
            findNavController().navigate(action)
        }

        dataViewModel.lifecycles.observe(viewLifecycleOwner) { lifecycles ->
            // TODO: set 11 tv's to the 11 lifecycles
            tv01.text = lifecycles[0].first
            tv01.setOnClickListener {
                val action = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleDescriptionFragment(lifecycles[0].first, lifecycles[0].second)
                findNavController().navigate(action)
            }
            binding.tv02.text = lifecycles[1].first
            tv02.setOnClickListener {
                val action = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleDescriptionFragment(lifecycles[1].first, lifecycles[1].second)
                findNavController().navigate(action)
            }
            binding.tv03.text = lifecycles[2].first
            tv03.setOnClickListener {
                val action = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleDescriptionFragment(lifecycles[2].first, lifecycles[2].second)
                findNavController().navigate(action)
            }
            binding.tv04.text = lifecycles[3].first
            tv04.setOnClickListener {
                val action = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleDescriptionFragment(lifecycles[3].first, lifecycles[3].second)
                findNavController().navigate(action)
            }
            binding.tv05.text = lifecycles[4].first
            tv05.setOnClickListener {
                val action = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleDescriptionFragment(lifecycles[4].first, lifecycles[4].second)
                findNavController().navigate(action)
            }
            binding.tv06.text = lifecycles[5].first
            tv06.setOnClickListener {
                val action = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleDescriptionFragment(lifecycles[5].first, lifecycles[5].second)
                findNavController().navigate(action)
            }
            binding.tv07.text = lifecycles[6].first
            tv07.setOnClickListener {
                val action = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleDescriptionFragment(lifecycles[6].first, lifecycles[6].second)
                findNavController().navigate(action)
            }
            binding.tv08.text = lifecycles[7].first
            tv08.setOnClickListener {
                val action = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleDescriptionFragment(lifecycles[7].first, lifecycles[7].second)
                findNavController().navigate(action)
            }
            binding.tv09.text = lifecycles[8].first
            tv09.setOnClickListener {
                val action = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleDescriptionFragment(lifecycles[8].first, lifecycles[8].second)
                findNavController().navigate(action)
            }
            binding.tv10.text = lifecycles[9].first
            tv10.setOnClickListener {
                val action = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleDescriptionFragment(lifecycles[9].first, lifecycles[9].second)
                findNavController().navigate(action)
            }
            binding.tv11.text = lifecycles[10].first
            tv11.setOnClickListener {
                val action = LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleDescriptionFragment(lifecycles[10].first, lifecycles[10].second)
                findNavController().navigate(action)
            }
        }

    }
}