package com.philroy.coroutine_intro.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.philroy.coroutine_intro.databinding.FragmentSpecificColorBinding

class SpecificColorFragment : Fragment() {

    private var _binding: FragmentSpecificColorBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSpecificColorBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val args: SpecificColorFragmentArgs by navArgs()
        super.onViewCreated(view, savedInstanceState)
        binding.root.setBackgroundColor(args.colorInt)

        binding.btnBack.setOnClickListener {
            val action = SpecificColorFragmentDirections.actionSpecificColorFragmentToColorFragment()
            findNavController().navigate(action)
        }
    }

}