package com.philroy.coroutine_intro.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.philroy.coroutine_intro.model.DataRepo
import kotlinx.coroutines.launch

class DataViewModel : ViewModel() {
    private val repo = DataRepo

    private var _randomColor = MutableLiveData<Int>()
    val randomColor : LiveData<Int> get() = _randomColor

    private val _lifecycles = MutableLiveData<List<Pair<String, String>>>()
    val lifecycles : LiveData<List<Pair<String, String>>> get() = _lifecycles

    fun getRandomColor() {
        viewModelScope.launch {
            _randomColor.value = repo.getRandomColor()
        }
    }

    fun getLifecycles() {
        viewModelScope.launch {
            _lifecycles.value = repo.getLifecycles()
        }
    }


}